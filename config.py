from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
import os
import subprocess


# setting some variables

mod = "mod4"
home = os.path.expanduser('~')
myterm = "xterm"
mybrowser = "firefox"
myeditor = "geany"
myfont = "Ubuntu Mono"

colors = ('#d3d0c8',  # foreground 0
          '#2d2d2d',  # background 1
          '#2d2d2d',  # black 1    2
          '#747369',  # black 2    3
          '#f2777a',  # red 1      4
          '#f2777a',  # red 2      5
          '#99cc99',  # green 1    6
          '#99cc99',  # green 2    7
          '#ffcc66',  # yellow 1   8
          '#ffcc66',  # yellow 2   9
          '#6699cc',  # blue 1     10
          '#6699cc',  # blue 2     11
          '#cc99cc',  # magenta 1  12
          '#cc99cc',  # magenta 2  13
          '#66cccc',  # cyan 1     14
          '#66cccc',  # cyan 2     15
          '#d3d0c8',  # white 1    16
          '#f2f0ec')  # white 2    17


blue = ('#104E8B',  # darkblue   0
        '#1874CD')  # brightblue 1

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Grow and shrink windows in XMonadTall
    Key([mod], "h", lazy.layout.shrink(),
        lazy.layout.decrease_nmaster()),
    Key([mod], "l", lazy.layout.grow(),
        lazy.layout.increase_nmaster()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "Tab", lazy.layout.next()),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout()),
    Key([mod], "q", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "s", lazy.shutdown()),
    Key([mod, "mod1"], "Return", lazy.spawncmd()),
]

groups = [
    Group("1"),
    Group("2", matches=[Match(wm_class=["lbry"])]),
    Group("3"),
    Group("4"),
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to
        # group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

layout_defaults = dict(
    border_width=2,
    border_focus=blue[1],
    border_normal=colors[2],
    margin=15
        )

layouts = [
    layout.Max(**layout_defaults),
    layout.MonadTall(**layout_defaults),
    ]

widget_defaults = dict(
    font=myfont,
    fontsize=16,
    padding=5,
    foreground=colors[0]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(background=blue[1], active=colors[16], inactive=colors[1], padding=3),
                widget.Prompt(),
                widget.WindowName(background=blue[0]),
                widget.Systray(background=blue[0]),
                widget.Clipboard(background=blue[1], foreground=colors[16], timeout=None),
                widget.CurrentLayout(background=blue[0], foreground=colors[16]),
                widget.CPU(background=blue[1], foreground=colors[16], format='{freq_current}GHz {load_percent}%'),
                widget.Memory(background=blue[0], foreground=colors[16]),
                widget.Net(background=blue[1], foreground=colors[16], format='{down} ↓↑ {up}'),
                widget.Clock(format='%H:%M %a %d.%m.%Y', background=blue[0], foreground=colors[16]),
            ],
            24, background=colors[1]
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'pavucontrol'},
    {'wmclass': 'gcolor2'},
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# Autostart #


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# Autostart End #


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
