#!/bin/sh
sxhkd -c ~/.dotfiles/.sxhkdrc &
picom --config ~/.config/picom.conf &
firefox &
lbry &
feh --bg-fill /usr/share/pixmaps/uni_wallpaper.jpg
