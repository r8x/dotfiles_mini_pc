
### Description of where to target the symlinks to ###

autostart.sh -> ~/.config/qtile
config.py -> ~/.config/qtile
.vimrc -> ~
.Xdefaults -> ~
.zshrc -> ~
keys.txt -> ~
compton.conf -> ~/.config
tmux.conf -> ~
