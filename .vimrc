" ---Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on


" ---Netrw
let g:netrw_liststyle = 3
let g:netrw_banner = 0

" ---EasyMotion
let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1

" ---Python Mode
let g:pymode = 1
let g:pymode_trim_whitespaces = 1
let g:pymode_indent = 1
let g:pymode_folding = 0
filetype off
call pathogen#infect()
call pathogen#helptags()
filetype plugin indent on
syntax on
let g:pymode_python = 'python3'


" ---Indent Guides
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
let g:indent_guides_guide_size = 1
hi IndentGuidesOdd ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey


" ---Airline
let g:airline_powerline_fonts=1
let g:airline_theme='dark'
let g:airline#extensions#branch#enabled = 1


" ---My Settings
set relativenumber
set number
set laststatus=2
set wildmenu
set cursorline
set hlsearch
set history=100
set showcmd
set scrolloff=10
set incsearch
set ignorecase
set smartcase
set nofoldenable
let mapleader=","

" ---Mappings
map <leader>w :w!<CR>
map <leader>q :q!<CR>
map <leader>wq :wq!<CR>
map <leader>x :only<CR>
map <leader>rl :edit!<CR>
map <leader>nrw :Explore<CR>
map <leader>hnrw :Sexplore<CR>
map <leader>vnrw :Vexplore<CR>
map <leader>lnt :PymodeLint<CR>
map <leader>pep :PymodeLintAuto<CR>
nmap üü O<ESC>
nmap ää o<ESC>
imap jj <ESC>
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)
nmap s <Plug>(easymotion-overwin-f2)
map <leader>h <Plug>(easymotion-linebackward)
map <leader>l <Plug>(easymotion-lineforward)
